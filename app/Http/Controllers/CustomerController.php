<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Yajra\DataTables\Facades\Datatables;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('customer.index');
    }

    public function getCustomers()
    {   
        $currentUser = session('authMsg');

        $responseUser = Http::withToken($currentUser['token'])->get(env('API_URL').'/api/v1/user');
        if($responseUser['success'])
        {
            $user = array_column($responseUser['message'], 'name', 'id');
        }
        
        $responseCustomer = Http::withToken($currentUser['token'])->get(env('API_URL').'/api/v1/customer');
        
        if($responseCustomer['success'])
        {
            $customers  = array_map(function ($n) use ($user)
            {
                return [
                
                    "id" => $n['id'],
                    "name" => "<a href='".route('customer.show', $n['id'])."'>".$n['name']."</a>",
                    "category" => $n['category'],
                    "domicile" => $n['domicile'],
                    "address" => $n['address'],
                    "email" => $n['email'],
                    "phone" => $n['phone'],
                    "lat" => $n['lat'],
                    "lng" => $n['lng'],
                    "status" => $n['isDisabled'] ? "Inactive" : "Active",
                    "credit_limit" => $n['credit_limit'],
                    "pic" => isset($user[$n['pic']]) ? $user[$n['pic']] : $n['pic'],
                    "action" => '<a href='.route('customer.show', $n['id']).' class="btn btn-xs btn-success" data-title="View Detail"><i class="fas fa-info-circle"></i></a> <a href="'.route('customer.edit', $n['id']).'" class="btn btn-xs btn-warning" data-title="Edit"><i class="fas fa-edit"></i></a></td>',
                    "created_at" => $n['created_at'],
                    "updated_at" => $n['updated_at']
                
                ];
            }, $responseCustomer['message']);
            
        }

        return Datatables::collection($customers)->rawColumns(['name', 'action'])->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = "Sales,Admin";
        $currentUser = session('authMsg');
        $responseUser = Http::withToken($currentUser['token'])->get(env('API_URL').'/api/v1/user/role/'.$role);
        $responseCategory = Http::withToken($currentUser['token'])->get(env('API_URL').'/api/v1/customer-category');

        return view('customer.create', [
            'pic' => $responseUser['message'],
            'customerCategory' =>  $responseCategory['message']
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required',
            'category' => 'required',
            'domicile' => 'required',
            // 'address' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'credit_limit' => 'required',
            'pic' => 'required',
        ]);

        $currentUser = session('authMsg');
        $request['isDisabled'] = isset($request['isDisabled']) ? true : false;

        if(isset($request['isEdit'])){

            $data = $request->except(['_token', 'isEdit']);
            $updatedBy = [
                'updated_by' => $currentUser['id'],
            ];
            $data = array_merge($data, $updatedBy);

            $response = Http::withToken($currentUser['token'])->put(env('API_URL').'/api/v1/customer/'.$data['id'], $data);

            if(!$response['success'])
            {
                return back()->withInput();
            }
            else
            {
                return redirect()->route('customer.index');
            }
            
            
        }
        else{
            $data = $request->except(['_token']);
            $createdUpdatedBy = [
                'created_by' => $currentUser['id'],
                'updated_by' => $currentUser['id'],
            ];
            $data = array_merge($data, $createdUpdatedBy);

            $response = Http::withToken($currentUser['token'])->post(env('API_URL').'/api/v1/customer', $data);
            if(!$response['success'])
            {
                return back()->withInput();
            }
            else
            {
                return redirect()->route('customer.index');
            }
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = "Sales,Admin";
        
        $currentUser = session('authMsg');
        $responseCustomer = Http::withToken($currentUser['token'])->get(env('API_URL').'/api/v1/customer/'.$id);
        $responseUser = Http::withToken($currentUser['token'])->get(env('API_URL').'/api/v1/user/role/'.$role);
        $responseCategory = Http::withToken($currentUser['token'])->get(env('API_URL').'/api/v1/customer-category');
        
        $customerCategory = array_column($responseCategory['message'], 'name', 'id');

        return view('customer.show', [
            'customer' => $responseCustomer['message'],
            'pic' => $responseUser['message'],
            'customerCategory' =>  $customerCategory
            ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = "Sales,Admin";
        
        $currentUser = session('authMsg');
        $responseCustomer = Http::withToken($currentUser['token'])->get(env('API_URL').'/api/v1/customer/'.$id);
        $responseUser = Http::withToken($currentUser['token'])->get(env('API_URL').'/api/v1/user/role/'.$role);
        $responseCategory = Http::withToken($currentUser['token'])->get(env('API_URL').'/api/v1/customer-category');

        return view('customer.edit', [
            'customer' => $responseCustomer['message'],
            'pic' => $responseUser['message'],
            'customerCategory' =>  $responseCategory['message']
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'name' => 'required',
            'category' => 'required',
            'domicile' => 'required',
            // 'address' => 'required',
            'phone' => 'required',
            'email' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'credit_limit' => 'required',
            'pic' => 'required',
            'isDisabled' => 'required',
        ]);
        $currentUser = session('authMsg');
        $createdUpdatedBy = [
            'created_by' => $currentUser['id'],
            'updated_by' => $currentUser['id'],
        ];
        $data = $request->except(['_token']);
        $data = array_merge($data, $createdUpdatedBy);

        $response = Http::withToken($currentUser['token'])->post(env('API_URL').'/api/v1/customer', $data);
        if(!$response['success'])
        {
            return back()->withInput();
        }
        else
        {
            return redirect()->route('customer.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
