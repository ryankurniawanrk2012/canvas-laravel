<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class AuthJWTCustom 
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        $user = session('authMsg');
        if(!$user)
        {
            return redirect('login');
        }
        $response = Http::withToken($user['token'])->post(env('API_URL').'/api/v1/check-login');
        $respArr = $response->json();

        if($respArr['success']){
            return $next($request);
        }
        return redirect('login');
    }   
}
