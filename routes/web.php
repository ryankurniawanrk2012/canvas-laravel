<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();



Route::middleware(['authjwtcostum'])->group(function () {
    Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

    //User
    // Route::resource('user', App\Http\Controllers\UserController::class);

    //Customer
    Route::resource('customer', App\Http\Controllers\CustomerController::class);
    Route::get('customers/get', [App\Http\Controllers\CustomerController::class, 'getCustomers'])->name('customer.getCustomers');

    //Inventory
    Route::resource('inventory', App\Http\Controllers\InventoryController::class);
    Route::get('inventories/get', [App\Http\Controllers\InventoryController::class, 'getInventories'])->name('inventory.getInventories');
});