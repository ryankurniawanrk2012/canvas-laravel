@extends('adminlte::page')

@section('title', 'Inventory')

@section('content_header')
<h1>Inventory</h1>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <a href="{{route('customer.create')}}" class="btn btn-primary mr-2">Add New</a>
    </div>
    <div class="card-body">
        <table id="allInventory" class="table table-bordered text-nowrap" style="width:100%">
            <thead class="thead-light">
                <tr>
                    <!--<th>&nbsp;</th>-->
                    <th>Name</th>
                    <th>Category</th>
                    <th>Unit Price</th>
                    <th>Quantity</th>
                    <th>Unit Type</th>
                    <th>Status</th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    $('#allInventory').DataTable( {  
        "fnDrawCallback": function( oSettings ) {                     
                    $('[data-toggle="tooltip"]').tooltip({
                        trigger : 'hover'
                    });   
                },
        select:{
            style:     'os',
            className: 'row-selected'
        },
        responsive: true,
        "language": {
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '
        },
        "scrollX":true,
        "fixedColumns": {
            "leftColumns": 3
        },
        "processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "{{ route('inventory.getInventories') }}",
                    "dataType": "json",
                    "type": "GET",
                    "data":{ _token: "{{csrf_token()}}"},
                    
                },
        "autoWidth" : true,
        "columns": [
            { "data": "name", "name": 'name' },
            { "data": "category", "name": 'category' },
            { "data": "unit_price", "name": 'unit_price' },
            { "data": "quantity", "name": 'quantity' },
            { "data": "unit_type", "name": 'unit_type' },
            { "data": "isDisabled", "name": 'isDisabled' },
        ],

    })
</script>
@stop