@extends('adminlte::page')

@section('title', 'Edit Customers')

@section('content_header')
<h1>Edit Customers</h1>
@stop

@section('content')

<div class="card">
    <div class="card-body">
        <div class="box box-primary">
            <!-- form start -->
            <form method="POST" action="{{route('customer.store')}}">
            @csrf
                <div class="box-body">
                    <input type="hidden" name="isEdit" value="true">
                    <input type="hidden" name="id" value="{{$customer['id']}}">
                    <div class="form-group">
                        <label for="nameInput">Nama</label>
                        <input type="input" class="form-control" name="name" id="nameInput" placeholder="Nama" @if(!empty(old('name'))) value="{{ old('name') }}" @else value="{{ $customer['name'] }}" @endif>
                    </div>
                    <div class="form-group">
                        <label for="categoryInput">Kategori</label>
                        <select name="category" class="form-control">
                            <option value="" selected disabled hidden>Kategori</option>
                            @foreach ($customerCategory as $cC)
                            <option value="{{$cC['id']}}" @if(!empty(old('category')) && old('category') == $cC['id']) selected @elseif($customer['category'] == $cC['id']) selected @endif>{{$cC['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="domisiliInput">Domisili</label>
                        <input type="input" class="form-control" name="domicile" id="domisiliInput" placeholder="Domisili" @if(!empty(old('domicile'))) value="{{ old('domicile') }}" @else value="{{ $customer['domicile'] }}" @endif>
                    </div>
                    <div class="form-group">
                        <label for="addressInput">Alamat</label>
                        <input type="input" class="form-control" name="address" id="addressInput" placeholder="Alamat" @if(!empty(old('address'))) value="{{ old('address') }}" @else value="{{ $customer['address'] }}" @endif>
                    </div>
                    <div class="form-group">
                        <label for="phoneInput">Phone Number</label>
                        <input type="input" class="form-control" name="phone" id="phoneInput" placeholder="Phone Number"  @if(!empty(old('phone'))) value="{{ old('phone') }}" @else value="{{ $customer['phone'] }}" @endif>
                    </div>
                    <div class="form-group">
                        <label for="emailInput">Email</label>
                        <input type="email" class="form-control" name="email" id="emailInput" placeholder="Email" @if(!empty(old('email'))) value="{{ old('email') }}" @else value="{{ $customer['email'] }}" @endif>
                    </div>
                    <div class="form-group">
                        <label for="latitudeInput">Latitude</label>
                        <input type="input" class="form-control" name="lat" id="latitudeInput" placeholder="Latitude" @if(!empty(old('lat'))) value="{{ old('lat') }}" @else value="{{ $customer['lat'] }}" @endif>
                    </div>
                    <div class="form-group">
                        <label for="longitudeInput">Longitude</label>
                        <input type="input" class="form-control" name="lng" id="longitudeInput" placeholder="Longitude" @if(!empty(old('lng'))) value="{{ old('lng') }}" @else value="{{ $customer['lng'] }}" @endif>
                    </div>
                    <div class="form-group">
                        <label for="creditLimitInput">Credit Limit</label>
                        <input type="input" class="form-control" name="credit_limit" id="creditLimitInput" placeholder="Credit Limit" @if(!empty(old('credit_limit'))) value="{{ old('credit_limit') }}" @else value="{{ $customer['credit_limit'] }}" @endif>
                    </div>
                    <div class="form-group">
                        <label for="categoryInput">PIC</label>
                        <select name="pic" class="form-control">
                            <option value="" selected disabled hidden>PIC</option>
                            @foreach ($pic as $p)
                            <option value="{{ $p['id'] }}" @if(!empty(old('pic')) && old('pic') == $p['id']) selected @elseif($customer['pic'] == $p['id']) selected @endif>{{ $p['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="checkbox">
                    <label>
                        <input type="checkbox" name="isDisabled" @if($customer['isDisabled']) checked @endif> Disabled
                    </label>
                    </div>
                </div>
              <!-- /.box-body -->

                <div class="box-footer">
                    
                    <button class="btn btn-default" style="min-width: 70px;">Cancel</button>
                    <button type="submit" class="btn btn-primary" style="min-width: 70px; float:right;">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
@stop