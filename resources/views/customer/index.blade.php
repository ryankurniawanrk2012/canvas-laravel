@extends('adminlte::page')

@section('title', 'Customers')

@section('content_header')
<h1>Customers</h1>
@stop

@section('content')
<div class="card">
    <div class="card-header">
        <a href="{{route('customer.create')}}" class="btn btn-primary mr-2">Add New</a>
    </div>
    <div class="card-body">
        <table id="allCustomer" class="table table-bordered text-nowrap" style="width:100%">
            <thead class="thead-light">
                <tr>
                    <!--<th>&nbsp;</th>-->
                    <th>Name</th>
                    <th>Category</th>
                    <th>Domicile</th>
                    <th>Address</th>
                    <th>Phone</th>
                    <th>Email</th>
                    <th>Credit Limit</th>
                    <th>PIC</th>
                    <th>Status</th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    $('#allCustomer').DataTable( {  
        "fnDrawCallback": function( oSettings ) {                     
                    $('[data-toggle="tooltip"]').tooltip({
                        trigger : 'hover'
                    });   
                },
        select:{
            style:     'os',
            className: 'row-selected'
        },
        responsive: true,
        "language": {
            processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading..n.</span> '
        },
        "scrollX":true,
        "fixedColumns": {
            "leftColumns": 3
        },
        "processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "{{ route('customer.getCustomers') }}",
                    "dataType": "json",
                    "type": "GET",
                    "data":{ _token: "{{csrf_token()}}"},
                    
                },
        "autoWidth" : true,
        "columns": [
            { "data": "name", "name": 'name' },
            { "data": "category", "name": 'category' },
            { "data": "domicile", "name": 'domicile' },
            { "data": "address", "name": 'address' },
            { "data": "phone", "name": 'phone' },
            { "data": "email", "name": 'email' },
            { "data": "credit_limit", "name": 'credit_limit' },
            { "data": "pic", "name": 'pic' },
            { "data": "status", "name": 'status' },
            { "data": "action", "name": 'action' },
        ],

    })
</script>
@stop