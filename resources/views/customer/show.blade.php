@extends('adminlte::page')

@section('title', 'Customer Details')

@section('content_header')
<h1>Customer Details</h1>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        <dl class="row">
            <dt class="col-sm-4">Nama:</dt><dd class="col-sm-8">{{ $customer['name'] }}</dd>
            <dt class="col-sm-4">Kategori:</dt><dd class="col-sm-8">{{ $customerCategory[$customer['category']] }}</dd>
            <dt class="col-sm-4">Domisili:</dt><dd class="col-sm-8">{{ $customer['domicile'] }}</dd>
            <dt class="col-sm-4">Alamat:</dt><dd class="col-sm-8">{{ $customer['address'] }}</dd>
            <dt class="col-sm-4">Phone Number:</dt><dd class="col-sm-8">{{ $customer['phone'] }}</dd>
            <dt class="col-sm-4">Email</dt><dd class="col-sm-8">{{ $customer['email'] }}</dd>
            <dt class="col-sm-4">Latitude</dt><dd class="col-sm-8">{{ $customer['lat'] }}</dd>
            <dt class="col-sm-4">Credit Limit</dt><dd class="col-sm-8">{{ $customer['lng'] }}</dd>
            <dt class="col-sm-4">PIC</dt><dd class="col-sm-8">{{ $customer['pic'] }}</dd>
            <dt class="col-sm-4">Status</dt><dd class="col-sm-8">{{ $customer['isDisabled'] ? 'Inactive' : 'Active' }}</dd>
        </dl>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
@stop