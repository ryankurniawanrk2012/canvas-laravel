@extends('adminlte::page')

@section('title', 'Add Customers')

@section('content_header')
<h1>Add Customers</h1>
@stop

@section('content')
<div class="card">
    <div class="card-body">
        <div class="box box-primary">
            <!-- form start -->
            <form method="POST" action="{{route('customer.store')}}">
            @csrf
                <div class="box-body">
                    <div class="form-group">
                        <label for="nameInput">Nama</label>
                        <input type="input" class="form-control" name="name" id="nameInput" placeholder="Nama">
                    </div>
                    <div class="form-group">
                        <label for="categoryInput">Kategori</label>
                        <select name="category" class="form-control">
                            <option value="" selected disabled hidden>Kategori</option>
                            @foreach ($customerCategory as $cC)
                            <option value="{{$cC['id']}}">{{$cC['name']}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="domisiliInput">Domisili</label>
                        <input type="input" class="form-control" name="domicile" id="domisiliInput" placeholder="Domisili">
                    </div>
                    <div class="form-group">
                        <label for="addressInput">Alamat</label>
                        <input type="input" class="form-control" name="address" id="addressInput" placeholder="Alamat">
                    </div>
                    <div class="form-group">
                        <label for="phoneInput">Phone Number</label>
                        <input type="input" class="form-control" name="phone" id="phoneInput" placeholder="Phone Number">
                    </div>
                    <div class="form-group">
                        <label for="emailInput">Email</label>
                        <input type="email" class="form-control" name="email" id="emailInput" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="latitudeInput">Latitude</label>
                        <input type="input" class="form-control" name="lat" id="latitudeInput" placeholder="Latitude">
                    </div>
                    <div class="form-group">
                        <label for="longitudeInput">Longitude</label>
                        <input type="input" class="form-control" name="lng" id="longitudeInput" placeholder="Longitude">
                    </div>
                    <div class="form-group">
                        <label for="creditLimitInput">Credit Limit</label>
                        <input type="input" class="form-control" name="credit_limit" id="creditLimitInput" placeholder="Credit Limit">
                    </div>
                    <div class="form-group">
                        <label for="categoryInput">PIC</label>
                        <select name="pic" class="form-control">
                            <option value="" selected disabled hidden>PIC</option>
                            @foreach ($pic as $p)
                            <option value="{{ $p['id'] }}">{{ $p['name'] }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="checkbox">
                    <label>
                        <input type="checkbox" name="isDisabled"> Disabled
                    </label>
                    </div>
                </div>
              <!-- /.box-body -->

                <div class="box-footer">
                    
                    <button class="btn btn-default" style="min-width: 70px;">Cancel</button>
                    <button type="submit" class="btn btn-primary" style="min-width: 70px; float:right;">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
@stop