@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
<h1>Dashboard</h1>
@stop

@section('content')
<div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">180 task completed out of 300</h3>
                <span style="float:right">
                    <select class="form-control">
                        <option>This Week</option>
                        <option>This Month</option>
                    </select>
                </span>
              </div>
              <!-- /.card-header -->
                <div class="card-body">
                    <div class="progress-group">
                    <div class="progress progress-sm">
                        <div class="progress-bar bg-primary" style="width: 80%"></div>
                    </div>
                    </div>
                    <h4>Rabu, 23 Desember</h4>
                    
                    <ul class="pagination pagination-month justify-content-center">
                        <li class="page-item" style="width:100%">
                            <a class="page-link" href="#">
                                <p class="page-month">Mon</p>
                                <p class="page-year">21</p>
                            </a>
                        </li>
                        <li class="page-item active" style="width:100%">
                            <a class="page-link" href="#">
                                <p class="page-month">Tue</p>
                                <p class="page-year">22</p>
                            </a>
                        </li>
                        <li class="page-item" style="width:100%">
                            <a class="page-link" href="#">
                                <p class="page-month">Wed</p>
                                <p class="page-year">23</p>
                            </a>
                        </li>
                        <li class="page-item" style="width:100%">
                            <a class="page-link" href="#">
                                <p class="page-month">Thu</p>
                                <p class="page-year">24</p>
                            </a>
                        </li>
                        <li class="page-item" style="width:100%">
                            <a class="page-link" href="#">
                                <p class="page-month">Fri</p>
                                <p class="page-year">25 </p>
                            </a>
                        </li>
                        <li class="page-item" style="width:100%">
                            <a class="page-link" href="#">
                                <p class="page-month">Sat</p>
                                <p class="page-year">26</p>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Visit Dinglo Cafe</h3><br>
                            <a class="users-list-name">Tanggal: 23 Desember 2020</a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="brand-link ">
                                <img src="/img/user1-128x128.jpg" alt="AdminLTE" class="brand-image img-circle elevation-3" style="opacity:.8">
                                <span class="brand-text font-weight-light ">
                                    <b>Roni Suroni</b>
                                </span>
                                <span style="float:right"><small class="badge badge-success"> Completed</small></span>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Visit Toko Sukamaju</h3><br>
                            <a class="users-list-name">Tanggal: 23 Desember 2020</a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="brand-link ">
                                <img src="/img/user7-128x128.jpg" alt="AdminLTE" class="brand-image img-circle elevation-3" style="opacity:.8">
                                <span class="brand-text font-weight-light ">
                                    <b>Siti</b>
                                </span>
                                <span style="float:right"><small class="badge badge-success"> Completed</small></span>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Visit PT. Abadi Jaya Makmur</h3><br>
                            <a class="users-list-name">Tanggal: 23 Desember 2020</a>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="brand-link ">
                                <img src="/img/user4-128x128.jpg" alt="AdminLTE" class="brand-image img-circle elevation-3" style="opacity:.8">
                                <span class="brand-text font-weight-light ">
                                    <b>Jessica</b>
                                </span>
                                <span style="float:right"><small class="badge badge-success"> Completed</small></span>
                            </div>
                        </div>
                    </div>
                    <p class="text-center"><a href="#">Show more</a></p>
                </div>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (left) -->
          <!-- right column -->
          <div class="col-md-6">
            <!-- Form Element sizes -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Top Sales</h3>
                <span style="float:right">
                    <select class="form-control">
                        <option>Weekly</option>
                        <option>Monthly</option>
                    </select>
                </span>
              </div>
              <div class="card-body">
              <ol>
                <li>Roni Suroni</li>
                <li>Rexy Subroto</li>
                <li>Ade Perkasa</li>
                <li>Miko Garibaldi</li>
                <li>Isyanio Adriyendi</li>
              </ol>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Tasks</h3>
                <span style="float:right">
                    <select class="form-control">
                        <option>Weekly</option>
                        <option>Monthly</option>
                    </select>
                </span>
              </div>
              <div class="card-body">
              <div class="col-xs-6 col-md-3 text-center">
                  <div style="display:inline;width:120px;height:120px;"><canvas width="120" height="120"></canvas><input type="text" class="knob" value="30" data-width="120" data-height="120" data-fgcolor="#f56954" style="width: 64px; height: 40px; position: absolute; vertical-align: middle; margin-top: 40px; margin-left: -92px; border: 0px; background: none; font: bold 24px Arial; text-align: center; color: rgb(245, 105, 84); padding: 0px; appearance: none;"></div>

                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div>
@stop

@section('css')
<link rel="stylesheet" href="/css/admin_custom.css">
@stop

@section('js')
<script>
    console.log('Hi!'); 
</script>
@stop